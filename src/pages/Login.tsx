import { IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonImg, IonPage, IonRow, IonToolbar } from "@ionic/react";
import { arrowForwardOutline } from "ionicons/icons";
import styles from "../pages/Login.module.css";
import KeypadInputs from "../components/keypad/KeypadInputs";
import Keypad from "../components/keypad/Keypad";
import { useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";
import { Plugins } from "@capacitor/core";

const Login = () => {
  const correctCode = [5, 9, 2, 5];
  const [keypadValues, setKeypadValues] = useState<any>(["", "", "", ""]);
  const [activeIndex, setActiveIndex] = useState<any>(0);
  const successRef = useRef<any>();
  const { BiometricAuth } = Plugins;

  const [incorrect, setIncorrect] = useState<any>(false);
  const [correct, setCorrect] = useState<any>(false);

  const handleClick = (index: number, value: string) => {
    console.log(keypadValues);

    const tempValues = [...keypadValues];
    tempValues[index] = parseInt(value);

    setKeypadValues(tempValues);
    setActiveIndex((activeIndex: number) => activeIndex + 1);
  };

  const handleAlert = (index: number, value: string) => {
    console.log(keypadValues);

    window.alert("Requerir Outro Pin???????");
  };

  const handleRemove = () => {
    const tempValues = [...keypadValues];
    tempValues[activeIndex - 1] = "";

    setKeypadValues(tempValues);
    activeIndex > 0 && setActiveIndex((activeIndex: number) => activeIndex - 1);
    setIncorrect(false);
    setCorrect(false);
  };

  useEffect(() => {
    if (parseInt(activeIndex) === parseInt(keypadValues.length)) {
      var error = false;

      keypadValues.forEach((value: string, index: string | number) => {
        if (parseInt(value) !== parseInt(correctCode[index])) {
          error = true;
          return false;
        }
      });

      if (error) {
        setIncorrect(true);
      } else {
        setCorrect(true);

        setTimeout(() => {
          successRef.current.classList.remove("hidden");
          successRef.current.classList.add("success");
        }, 900);
      }
    }
  }, [activeIndex]);

  const loginWithFinger = async () => {
    const available = await BiometricAuth.isAvailable();
    const hasBiometricAuth = available.has;
    console.log("Tentou");

    if (hasBiometricAuth) {
      console.log("Tem Biometria");
      const authResult = await BiometricAuth.verify({
        reason: "Session expired",
        title: "Session timed out",
      });
      if (authResult.verified) {
        console.log("Logged In");
      }
    } else {
      console.log("Nao Tem Biometria");
    }
  };

  return (
    <IonPage>
      <IonContent>
        <IonGrid className="ion-text-center ">
          <IonRow style={{ paddingTop: "32%" }}>
            <IonCol size="12">
              <div
                onClick={() => {
                  loginWithFinger();
                }}
              >
                <IonImg src="/assets/impDig.png" className={styles.logo} />
              </div>
              <h1>Verification required</h1>
              <p>Enter your 4 digit verification code</p>
            </IonCol>
          </IonRow>
          <KeypadInputs incorrect={incorrect} correct={correct} values={keypadValues} activeIndex={activeIndex} />
          {incorrect && <p className={styles.incorrect}>Wrong code entered</p>}
          <Keypad
            handleRemove={handleRemove}
            handleClick={handleClick}
            handleAlert={handleAlert}
            activeIndex={activeIndex}
            amount={keypadValues.length}
            correct={correct}
          />
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default Login;
