import {
  IonButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonPage,
  IonRow,
  IonTitle,
  IonToolbar,
  useIonViewWillEnter,
} from "@ionic/react";

import React, { useState } from "react";
import { queryAllUsers, getUserById, deleteUserById } from "../dataservice";
import { CurrentUser } from "../components/CurrentUser";
import { UserList } from "../components/UserList";
import { useHistory } from "react-router";

const Home: React.FC = () => {
  const [queryResults, setQueryResults] = useState<any>(null);
  const [currentUser, setCurrentUser] = useState<any>(null);
  const history = useHistory();

  useIonViewWillEnter(() => {
    queryAllUsers().then(setQueryResults);
    setCurrentUser(null);
  });

  /**
   *
   * @param userId
   */
  const getById = async (userId: any) => {
    const c = await getUserById(userId);
    setCurrentUser(null);
    history.push("/login");
  };

  /**
   *
   * @param userId
   */
  const deleteUser = async (userId: any) => {
    if (window.confirm("Você realmente quer eliminar o utilizador?")) {
      await deleteUserById(userId);
      const data = await queryAllUsers();
      setQueryResults(data);
      setCurrentUser(null);
      window.alert("DELETE SUCCESS");
    }
  };

  return (
    <IonPage>
      <IonContent fullscreen className="ion-padding">
        <CurrentUser user={currentUser} onDelete={deleteUser} onEdit={(id: any) => history.push(`/edit-user/${id}`)} />
        <UserList
          users={queryResults}
          onDelete={deleteUser}
          onEdit={(id: any) => history.push(`/edit-user/${id}`)}
          userClicked={getById}
        />
      </IonContent>
    </IonPage>
  );
};

export default Home;
